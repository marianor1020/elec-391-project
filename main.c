/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define Nwindow 93

#define CW 0
#define CCW 1
#define SWITCHING_OFFSET 7

#define KP	7
#define KI	3
#define KD	0

#define dt  0.001
#define filterSize 4
#define epsilon 0.01
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
/* USER CODE BEGIN PFP */

void motorDrive0(void);
void PID0(void);
int8_t CheckEncoder(uint8_t, uint8_t, uint8_t, uint8_t);
float Lowest_Error(float, float);
double Derivative_Filter0(double);
void reluctanceDrive0(uint8_t);
float angleFormat(float);
uint8_t reluctancePosition(int16_t, float);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t Rx_Data[3];
uint8_t Tx_Data[20] = "Starting \n\r";

volatile int16_t PWM0 = 0;
volatile float Desired_Angle0 = 0, angle0 = 0, Desired_Angle1 = 0, angle1 = 0;
volatile uint8_t encoderFlag1, encoderFlag2, previousEncoderFlag1, previousEncoderFlag2;
volatile uint8_t encoderFlag3, encoderFlag4, previousEncoderFlag3, previousEncoderFlag4;
volatile int16_t opticalEncoder0 = 0, opticalEncoder1 = 0;
volatile uint8_t motor0_state = 0;
volatile int testVal = CW;
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	float tempAngle = 0;
  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  /* USER CODE BEGIN 2 */
	HAL_TIM_PWM_Start_IT(&htim1,TIM_CHANNEL_1);
	HAL_UART_Receive_IT(&huart2, Rx_Data, 3);	//Start USART connection as INT
	HAL_TIM_Base_Start_IT(&htim2);	//Start TIM2 to generate INT
	HAL_TIM_Base_Start_IT(&htim3);	//Start TIM2 to generate INT
	
	HAL_TIM_PWM_Start_IT(&htim4, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start_IT(&htim4, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start_IT(&htim4, TIM_CHANNEL_4);
	
	encoderFlag1 = HAL_GPIO_ReadPin(Encoder_0_GPIO_Port, Encoder_0_Pin);
	encoderFlag2 = HAL_GPIO_ReadPin(Encoder_1_GPIO_Port, Encoder_1_Pin);
	encoderFlag3 = HAL_GPIO_ReadPin(Encoder_2_GPIO_Port, Encoder_2_Pin);
	encoderFlag4 = HAL_GPIO_ReadPin(Encoder_3_GPIO_Port, Encoder_3_Pin);

	previousEncoderFlag1 = encoderFlag1;
	previousEncoderFlag2 = encoderFlag2;
	previousEncoderFlag3 = encoderFlag3;
	previousEncoderFlag4 = encoderFlag4;

	reluctanceDrive0(motor0_state);
  /* USER CODE END 2 */
 
 

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		
		tempAngle = fmod(opticalEncoder0 * 360.0/(Nwindow*4), 360);
		angle0 = angleFormat(tempAngle);
		
		tempAngle = fmod(opticalEncoder1 * 360.0/(Nwindow*4), 360);
		angle1 = angleFormat(tempAngle);
		
		sprintf((char*)Tx_Data, "%3.3f %3.3f %3.3d %d \r\n", angle0, Desired_Angle0, PWM0, motor0_state);
		//sprintf((char*)Tx_Data, "%3.3f %3.3d %d %d %d \r\n", angle0, encoderFlag1, previousEncoderFlag1, encoderFlag2, previousEncoderFlag2);
		HAL_UART_Transmit(&huart2, Tx_Data, strlen((char*)Tx_Data), 255);
		
//		if(HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin) != GPIO_PIN_SET)
//		{	
//			while (!(HAL_GPIO_ReadPin (B1_GPIO_Port, B1_Pin)));
//		}
		
		motor0_state = reluctancePosition(PWM0, angle0);
		reluctanceDrive0(motor0_state);
		
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 4200;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 8400;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 10;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{
  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 8400;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 250;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

 /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 420;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 1000;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 200;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, Phase_C_Pin|Phase_B_Pin|Phase_A_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4|GPIO_PIN_5, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : Encoder_0_Pin Encoder_1_Pin */
  GPIO_InitStruct.Pin = Encoder_0_Pin|Encoder_1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : Phase_C_Pin Phase_B_Pin Phase_A_Pin */
  GPIO_InitStruct.Pin = Phase_C_Pin|Phase_B_Pin|Phase_A_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : Encoder_2_Pin */
  GPIO_InitStruct.Pin = Encoder_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Encoder_2_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : Encoder_3_Pin */
  GPIO_InitStruct.Pin = Encoder_3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Encoder_3_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PB4 PB5 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI1_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */
//Motor driver
void motorDrive0(void){
	htim1.Instance->CCR1 = abs(PWM0);
	
	if(PWM0>0){	//CW
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);
	}
	else{				//CCW
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_SET);
	}
}

void reluctanceDrive0(uint8_t state)
{
	switch(state){
		case 0:
			//HAL_GPIO_WritePin(Phase_A0_GPIO_Port, Phase_A0_Pin, GPIO_PIN_SET);
			htim4.Instance->CCR1 = abs(PWM0);
			htim4.Instance->CCR3 = 0;
			htim4.Instance->CCR4 = 0;
			break;
		case 1:
			//HAL_GPIO_WritePin(Phase_B0_GPIO_Port, Phase_B0_Pin, GPIO_PIN_SET);
			htim4.Instance->CCR1 = 0;
			htim4.Instance->CCR3 = 0;
			htim4.Instance->CCR4 = abs(PWM0);
			break;
		case 2:
			//HAL_GPIO_WritePin(Phase_C0_GPIO_Port, Phase_C0_Pin, GPIO_PIN_SET);
			htim4.Instance->CCR1 = 0;
			htim4.Instance->CCR3 = abs(PWM0);
 			htim4.Instance->CCR4 = 0;
			break;
	}
}

uint8_t reluctancePosition(int16_t motorPWM, float motorAngle)
{
	uint8_t dir = (motorPWM < 0)?CW:CCW;
	uint8_t state;
	
	if(motorAngle >= 0   && motorAngle < 30  ){
		if(dir == CCW){
			if (motorAngle > (30 - SWITCHING_OFFSET))
				state = 1;
			else
				state = 2;
		}
		else {
			if((motorAngle < SWITCHING_OFFSET + 0))
				state = 1;
			else
				state = 0;
		}
	}
	else if(motorAngle >= 30  && motorAngle < 60 ){
		if(dir == CCW){
			if (motorAngle > (60 - SWITCHING_OFFSET))
				state = 0;
			else
				state = 1;
		}
		else {
			if(motorAngle < (30 + SWITCHING_OFFSET))
				state = 0;
			else
				state = 2;
		}
	}
	else if(motorAngle >= 60  && motorAngle < 90 ){
		if(dir == CCW){
			if (motorAngle > (90 - SWITCHING_OFFSET))
				state = 2;
			else
				state = 0;
		}
		else {
			if(motorAngle < (60 + SWITCHING_OFFSET))
				state = 2;
			else
				state = 1;
		}
	}
	else if(motorAngle >= 90  && motorAngle < 120 ){
		if(dir == CCW){
			if (motorAngle > (120 - SWITCHING_OFFSET))
				state = 1;
			else
				state = 2;
		}
		else {
			if(motorAngle < (90 + SWITCHING_OFFSET))
				state = 1;
			else
				state = 0;
		}
	}
	else if (motorAngle >= 120  && motorAngle < 150 ){
		if(dir == CCW){
			if (motorAngle > (150 - SWITCHING_OFFSET))
				state = 0;
			else
				state = 1;
		}
		else {
			if(motorAngle < (120 + SWITCHING_OFFSET))
				state = 0;
			else
				state = 2;
		}
	}
	else if(motorAngle >= 150  && motorAngle < 180 ){
		if(dir == CCW){
			if (motorAngle > (180 - SWITCHING_OFFSET))
				state = 2;
			else
				state = 0;
		}
		else {
			if(motorAngle < (150 + SWITCHING_OFFSET))
				state = 2;
			else
				state = 1;
		}
	}
	else if(motorAngle >= -180 && motorAngle < -150){
		if(dir == CCW){
			if (motorAngle > (-150 - SWITCHING_OFFSET))
				state = 1;
			else
				state = 2;
		}
		else {
			if(motorAngle < (-180 + SWITCHING_OFFSET))
				state = 1;
			else
				state = 0;
		}
	}
	else if(motorAngle >= -150 && motorAngle < -120){
		if(dir == CW){
			if (motorAngle > (-120 - SWITCHING_OFFSET))
				state = 0;
			else
				state = 1;
		}
		else {
			if(motorAngle < (-150 + SWITCHING_OFFSET))
				state = 0;
			else
				state = 2;
		}
	}
	else if(motorAngle >= -120 && motorAngle < -90){
		if(dir == CCW){
			if (motorAngle > (-90 - SWITCHING_OFFSET))
				state = 2;
			else
				state = 0;
		}
		else {
			if(motorAngle < (-120 + SWITCHING_OFFSET))
				state = 2;
			else
				state = 1;
		}
	}
	else if(motorAngle >= -90 && motorAngle < -60){
		if(dir == CCW){
			if (motorAngle > (-60 - SWITCHING_OFFSET))
				state = 1;
			else
				state = 2;
		}
		else {
			if(motorAngle < (-90 + SWITCHING_OFFSET))
				state = 1;
			else
				state = 0;
		}
	}
	else if(motorAngle >= -60 && motorAngle < -30){
		if(dir == CCW){
			if (motorAngle > (-30 - SWITCHING_OFFSET))
				state = 0;
			else
				state = 1;
		}
		else {
			if(motorAngle < (-60 + SWITCHING_OFFSET))
				state = 0;
			else
				state = 2;
		}
	}
	else if(motorAngle >= -30 && motorAngle < 0){
		if(dir == CCW){
			if (motorAngle > (0 - SWITCHING_OFFSET))
				state = 2;
			else
				state = 0;
		}
		else {
			if(motorAngle < (-30 + SWITCHING_OFFSET))
				state = 2;
			else
				state = 1;
		}
	}
	else
		state = 3;
	return state;
}

void PID0(void)
{
	static double	Previous_error = 0; 
	static double Total_error = 0;
	
	double derivativeTerm;
	double error;
	
	error = Lowest_Error(Desired_Angle0, angle0);
	
	PWM0 = KP * error;
	
	if(error>epsilon || error < -epsilon)
		Total_error += error*dt;
	
	PWM0 += KI * Total_error;
	
	derivativeTerm = (error - Previous_error)/dt; //INT every 1ms
	PWM0 += KD * derivativeTerm; //Derivative_Filter0(derivativeTerm);
	
	Previous_error = error;
	
	if(PWM0>1000)
		PWM0 = 1000;
	else if(PWM0<-1000)
		PWM0 = -1000;

}

float Lowest_Error(float desired, float actual)
{
	float error1 = desired - actual;
	float error2 = 0;
	float desiredNew, actualNew;
	
	if(desired < 0)
		desiredNew = desired + 360;
	else
		desiredNew = desired;
	
	if(actual < 0)
		actualNew = actual + 360;
	else
		actualNew = actual;
	
	error2 = desiredNew - actualNew;
	
	if(abs((int)error1) <= abs((int)error2))
		return error1;
	else
		return error2;
}

double Derivative_Filter0(double currentDerivative)
{
	static uint8_t AverageArrayIndex = 0;
	static double DerivativeAverage[filterSize];	
	double average = 0;
	int index;
	
	DerivativeAverage[AverageArrayIndex] = currentDerivative;
	AverageArrayIndex = (AverageArrayIndex+1)%filterSize;
	
	for(index = 0; index<filterSize; index++){
		average += DerivativeAverage[index];
	}
	return average/filterSize;
}

int8_t CheckEncoder(uint8_t shiftFlag1, uint8_t shiftFlag2, uint8_t shiftPreviousFlag1, uint8_t shiftPreviousFlag2)
{
	
	uint8_t direction;
	int8_t opticalEncoder = 0;
	
	if (shiftPreviousFlag1 == 0 && shiftPreviousFlag2 == 0){
		if(shiftFlag1 == 1 && shiftFlag2 == 0)
			direction = CW;
		
		else if(shiftFlag1 == 0 && shiftFlag2 == 1)
			direction = CCW;
	}
	else if(shiftPreviousFlag1 == 0 && shiftPreviousFlag2 == 1){
		if(shiftFlag1 == 0 && shiftFlag2 == 0)
			direction = CW;
		
		else if(shiftFlag1 == 1 && shiftFlag2 == 1)
			direction = CCW;
	}
	else if(shiftPreviousFlag1 == 1 && shiftPreviousFlag2 == 0){
		if(shiftFlag1 == 1 && shiftFlag2 == 1)
			direction = CW;
		
		else if(shiftFlag1 == 0 && shiftFlag2 == 0)
			direction = CCW;
	}
	else if(shiftPreviousFlag1 == 1 && shiftPreviousFlag2 == 1){
		if(shiftFlag1 == 0 && shiftFlag2 == 1)
			direction = CW;
		
		else if(shiftFlag1 == 1 && shiftFlag2 == 0)
			direction = CCW;
	}
	
	if(shiftFlag1 != shiftPreviousFlag1)
		opticalEncoder += (direction == CW)?1:-1;
	
	else if(shiftFlag2 != shiftPreviousFlag2)
		opticalEncoder += (direction == CW)?1:-1;
	
	return opticalEncoder;
}

float angleFormat(float angle)
{
	float outAngle;
	if( angle > 180)
		outAngle = angle - 360;
	else if(angle <= -180)
		outAngle = angle + 360;
	else
		outAngle = angle;
	
	return outAngle;
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);
	
	float tempangle = atoi((char*)Rx_Data)%360;
	
	Desired_Angle0 = angleFormat(tempangle);

	HAL_UART_Receive_IT(&huart2, Rx_Data, 3);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if(htim == &htim2){
		PID0();
		motorDrive0();
	}
	
	if(htim == &htim3){
		//motor0_state = (motor0_state+1)%3;
		//reluctanceDrive0(motor0_state);
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{ 
	if(GPIO_Pin == Encoder_0_Pin || GPIO_Pin == Encoder_1_Pin){
		encoderFlag1 = HAL_GPIO_ReadPin(Encoder_0_GPIO_Port, Encoder_0_Pin);
		encoderFlag2 = HAL_GPIO_ReadPin(Encoder_1_GPIO_Port, Encoder_1_Pin);

		opticalEncoder0 += CheckEncoder(encoderFlag1, encoderFlag2, previousEncoderFlag1, previousEncoderFlag2);

		previousEncoderFlag1 = encoderFlag1;
		previousEncoderFlag2 = encoderFlag2;
	}
	
//	if(GPIO_Pin == Encoder_2_Pin || GPIO_Pin == Encoder_3_Pin){
//		encoderFlag3 = HAL_GPIO_ReadPin(Encoder_2_GPIO_Port, Encoder_2_Pin);
//		encoderFlag4 = HAL_GPIO_ReadPin(Encoder_3_GPIO_Port, Encoder_3_Pin);

//		opticalEncoder1 += CheckEncoder(encoderFlag3, encoderFlag4, previousEncoderFlag3, previousEncoderFlag4);

//		previousEncoderFlag3 = encoderFlag3;
//		previousEncoderFlag4 = encoderFlag4;
//	}
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
